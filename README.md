# Prueba Iconos


## Getting started Windows
```
cmd mvnw.cmd

```

## Getting started Linux
```
bin/sh mvnw.sh
```

## Build Image
```
docker build -t tribal-icons .
```

## Run Container
```
docker run -it -p 8080:8080 tribal-icons
```