package com.tribal.co.pruebaiconos.service;

import com.tribal.co.pruebaiconos.ConsumeEndpoint.Iconos.ConsumeEndpoints;
import com.tribal.co.pruebaiconos.dto.IconDto;
import com.tribal.co.pruebaiconos.dto.IconDtoList;
import com.tribal.co.pruebaiconos.threads.IconThread;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@Data
@RequiredArgsConstructor
public class IconService {

    @Autowired
    final ConsumeEndpoints endpoint;

    List<IconThread> threadList;


    public IconDtoList findIconList() throws InterruptedException {
        IconDto [] icons = new IconDto[25];
        threadList = new ArrayList<>();
        for(int i = 0;i <= 24;i++){
            threadList.add(new IconThread(endpoint,icons,i,i));
            threadList.get(i).start();
        }
        for(int i = 0;i <= 24;i++){
            threadList.get(i).join();
        }

        return IconDtoList.builder()
                .icons(Arrays.asList(icons))
                .build();
    }
}

