package com.tribal.co.pruebaiconos.Controller;

import com.tribal.co.pruebaiconos.service.IconService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/")
public class Controller {

    final IconService service;


    @GetMapping("iconos")
    public ResponseEntity<?> findIconList() throws InterruptedException {
        return ResponseEntity.status(HttpStatus.OK)
                .body(service.findIconList());
    }

}
