package com.tribal.co.pruebaiconos.threads;

import com.tribal.co.pruebaiconos.ConsumeEndpoint.Iconos.ConsumeEndpoints;
import com.tribal.co.pruebaiconos.dto.IconDto;
import com.tribal.co.pruebaiconos.dto.IconDtoList;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Data
@AllArgsConstructor
public class IconThread extends Thread{

    ConsumeEndpoints endpoint;

    IconDto [] icons;

    int inicio;

    int fin;

    @SneakyThrows
    @Override
    public void run(){
        for (int i =inicio ; i <= fin;i++) {
            IconDto iconReturned = endpoint.findRandomIcon();
                if (icons[i] != iconReturned) {
                    icons[i] = iconReturned;
                }
        }

    }
}
