package com.tribal.co.pruebaiconos.ConsumeEndpoint.Iconos;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tribal.co.pruebaiconos.Commons.Constants;
import com.tribal.co.pruebaiconos.dto.IconDto;
import com.tribal.co.pruebaiconos.model.IconModel;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class ConsumeEndpoints {
    RestTemplate restTemplate = new RestTemplate();
    ObjectMapper objMapper = new ObjectMapper();

    public IconDto findRandomIcon(){
        ResponseEntity<IconModel> icon = restTemplate.getForEntity(Constants.ICON_ENDPOINT,IconModel.class);

                return objMapper.convertValue(icon.getBody(), IconDto.class);
    }
}
