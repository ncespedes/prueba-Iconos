package com.tribal.co.pruebaiconos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
public class PruebaIconosApplication {

    public static void main(String[] args) {
        SpringApplication.run(PruebaIconosApplication.class, args);
    }

}
