package com.tribal.co.pruebaiconos.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class IconModel {
    String id;
    @JsonProperty("icon_url")
    String iconUrl;
    String value;
}
