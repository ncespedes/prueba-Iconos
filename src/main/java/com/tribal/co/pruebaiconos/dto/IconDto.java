package com.tribal.co.pruebaiconos.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class IconDto {
    String id;
    @JsonProperty("icon_url")
    String iconUrl;
    String value;
}
