FROM maven:3-jdk-11-slim
VOLUME /tmp
COPY . .
RUN mvn clean install
COPY target/*.jar  app.jar
ENTRYPOINT ["java","-Dspring.profiles.active=dev","-jar","/app.jar"]
